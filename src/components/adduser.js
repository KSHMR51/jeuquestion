import React, { useState, useCallback, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import firebase from '../utils/base';
import { Button } from "react-bootstrap";

const AddUsers = () => {
  // Initialisation de mes useState
  const [pseudo, setPseudo] = useState("")
  const [joueurs, setJoueurs] = useState([])
  // Initialisation de mon history
  const history = useHistory()

  // Const qui va me set les informations que je souhaite dans firebase
  const handleSubmit = (e) => {
    e.preventDefault();
    const itemsRef = firebase.database().ref(`joueurs/${pseudo}`).set({
      pseudo: pseudo,
      progression: {
        last: 0,
        score: 0
      },
    });
    // Je réinisialise le state pseudo
    setPseudo("")
    history.push(`/${pseudo}`)
  }

  // je supprime les joueurs de ma base de donnée
  const removeJoueurs = useCallback(() => {
    const removeRef = firebase.database().ref('joueurs');
    removeRef.remove()
  })

  // const updateProgression = useCallback(() => {
  //   const updateRef = firebase.database().ref('joueurs')
  //   let updateProgression = {
  //     pseudo: pseudo,
  //     progression: {
  //       last: last,
  //       score: score,
  //     }
  //   }
  //   updateRef.child(updateJoueur.pseudo).update(updateJoueur);
  // }, [joueur])

  // Ici je viens récupéres tout les joueurs de la base de donnée
  const getJoueurs = useCallback(() => {
    const joueursRef = firebase.database().ref('joueurs').once('value').then(function (snapshot) {
      let joueurs = []
      snapshot.forEach(function (childSnap) {
        joueurs.push(childSnap.val())
      });
      // met à jour les joueurs dans mon state
      setJoueurs(joueurs)
    });
  }, [firebase])

  // le useEffect va cherché la liste des joueurs en 1er
  useEffect(() => {
    getJoueurs()
  }, [getJoueurs])

  return (
    <div className='app'>
      <div className='container'>
        <section className='add-item'>
          <form>
            <input className="mx-auto d-block" type="text" name="pseudo" placeholder="Votre pseudo" onChange={e => setPseudo(e.target.value)} value={pseudo} />
            <Button onClick={handleSubmit} className="mx-auto d-block my-4" variant="success">Add user</Button>
            <Button onClick={removeJoueurs} className="mx-auto d-block" variant="danger">Supprimer les utilisateurs</Button>
          </form>
        </section>
        <section className='display-item'>
          <div className='wrapper'>
            <ul>
              {
                joueurs.map((joueur) => (
                  <li key={joueur.pseudo}>
                    <Link to={`/${joueur.pseudo}`}>{joueur.pseudo}</Link>
                  </li>
                ))
              }
            </ul>
          </div>
        </section>
      </div>
    </div>
  );
}
export default AddUsers;