import React from 'react';
import { Container } from 'react-bootstrap';

// Je passe ici en paramètre les props dont j'ai besoin pour les recup sur la page
export const Questions = ({ question, joueur, action, score, children }) => {
  return (
    <Container>
      <header>
        <h2 className="text-center mt-5">Bonjour {joueur.pseudo}</h2>
        <p className="text-center">point {score}</p>
        <div className='wrapper'>
          <h1>Jeu des questions random</h1>
        </div>
      </header>
      <div className='container'>
        <section className='display-item'>
          <div className='wrapper'>
            <ul>
            </ul>
          </div>
        </section>
        <section className='display-item'>
          <div className="wrapper">
            Question n°{question.number}
            <h2>{question.question}</h2>
            <ul>
              {question.reponses.map(r => {
                return (
                  <li onClick={() => action(r)} key={r.id}>
                    {r.text}
                  </li>
                )
              })}
            </ul>
            {children}
          </div>
        </section>
      </div>
    </Container>
  );
}
