import React, { useState, useEffect, useCallback } from 'react';
import firebase from '../utils/base';
import { Spinner, ProgressBar, Container, Button } from "react-bootstrap"
import { Questions } from "../components/questions"
import { Score } from "../components/score"
import {
  Link,
  useParams
} from "react-router-dom";


export const Menu = () => {
  let { pseudo } = useParams()
  //initialisation de tout mes State
  const [questions, setQuestions] = useState([])
  const [question, setQuestion] = useState(null)
  const [joueur, setJoueur] = useState(null)
  const [response, setResponse] = useState(null)
  const [completed, setCompleted] = useState(false)

  // si la question est valide + mise à jour du joueur
  const validResponse = useCallback((completed) => {
    let last = question.number
    let score = joueur.progression.score = joueur.progression.score + (response.valid ? 1 : 0)
    let updateJoueur = {
      pseudo: joueur.pseudo,
      progression: {
        last: last,
        score: score,
      }
    }
    setJoueur(updateJoueur)
    saveJoueur(updateJoueur)
  }, [question, joueur, response])

  // J'update ici dans firebase mon utilisateur / je passe le updateJoueur en paramètre
  const saveJoueur = useCallback((updateJoueur) => {
    const updateRef = firebase.database().ref('joueurs')
    updateRef.child(updateJoueur.pseudo).update(updateJoueur);
  }, [joueur])

  // Ici je vais cherche mes questions dans firebase et je les ranges dans le bonne ordre
  const getQuestions = useCallback(() => {
    const questionsRef = firebase.database().ref('questions');
    questionsRef.on('value', (snapshot) => {
      let questions = snapshot.val();
      // Le sort va trier les questions par rapport à leur champ number
      questions = questions.sort((a, b) => (a.number > b.number) ? 1 : -1)
      // le filter vérifie bien le champ number
      questions = questions.filter(q => q.number)
      setQuestions(questions)
    });
  }, [])

  // Ici je viens set mon joueur 
  const getJoueur = useCallback(() => {
    const joueurRef = firebase.database().ref('joueurs')
    joueurRef.orderByChild("pseudo").equalTo(pseudo).on('child_added', function (snapshot) {
      let joueur = snapshot.val()
      setJoueur(joueur)
    })
  }, [])

  // j'initialise la questionn du joueur qui est en cours / si il a finis alors je set completed à true et qu'il a finis
  const initQuestion = useCallback(() => {
    let selectedQuestion = questions.find(q => q.number === joueur.progression.last + 1)
    if ((joueur.progression.last) >= questions.length) {
      setCompleted(true)
    }
    setQuestion(selectedQuestion)
  }, [joueur, questions])

  // Appelle qu'une seul fois quand le composent est monté
  useEffect(() => {
    getQuestions()
    getJoueur()
  }, [])

  useEffect(() => {
    if (joueur && questions.length) {
      initQuestion()
    }
  }, [joueur, questions])

  return (
    // je ramène la question qui est initialisé pour le joueur, si ce n'est pas completed alors j'appelle mon Container
    // Sinon le completed ce lance et je fait apparaitre le score
    question && !completed ?
      <Container>
        <Link to='/'>
          <Button className="mx-auto d-block mt-3" variant="dark">Accueil</Button>
        </Link>
        <ProgressBar variant="success" className="mt-5" animated now={question.number / questions.length * 100} />
        <Questions score={`${joueur.progression.score} / ${joueur.progression.last}`} question={question} joueur={joueur} action={setResponse}>
          {response && <Button onClick={() => validResponse(question.number === questions.length)}>valider ma reponse</Button>}
        </Questions>
      </Container>
      : completed ?
        <Score score={`${joueur.progression.score} / ${joueur.progression.last}`} />
        : <Spinner animation="border" />
  );
}
