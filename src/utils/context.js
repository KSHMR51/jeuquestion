import React, { useState, createContext } from 'react';

const CategoryContext = createContext({
  category: (false),
})

const CategoryProvider = props => {
  const [category, setCategory] = useState(false)
  const { children } = props

  return (
    <CategoryContext.Provider
      value={{
        category: category,
        setCategory: setCategory,
      }}
    >
      {children}
    </CategoryContext.Provider>
  )
}

export default CategoryContext

export { CategoryProvider }