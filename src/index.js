import React from 'react';
import ReactDOM from 'react-dom';
import { Menu } from './pages/menu';
import { App } from './pages/App';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';

const Root = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/">
        <App />
      </Route>
      <Route exact path="/:pseudo">
        <Menu />
      </Route>
    </Switch>
  </BrowserRouter>
);

ReactDOM.render(<Root />, document.getElementById("root"));
serviceWorker.unregister();

